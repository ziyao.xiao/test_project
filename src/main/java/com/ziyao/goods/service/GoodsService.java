package com.ziyao.goods.service;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GoodsService {

	@Autowired
	private RestTemplate restTemplate;
	
    @Value("${goods.url}")
	String goodsUrl;
	
	public Map findByIdGoods(String id) {
		ResponseEntity<Map> responseEntity = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map> request = new HttpEntity<>(null, headers);
		responseEntity = restTemplate.exchange(goodsUrl + "goodses/" + id, HttpMethod.GET, request, Map.class);
		Map res = responseEntity.getBody();
		return res;
	}
	
	public Map updateGoodsInventory(String goodsId, Map requestData) {
		ResponseEntity<Map> responseEntity = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map> request = new HttpEntity<>(requestData, headers);
		responseEntity = restTemplate.exchange(goodsUrl + "goods/" + goodsId + "/inventory", HttpMethod.PATCH, request, Map.class);
		Map res = responseEntity.getBody();
		return res;
	}
}

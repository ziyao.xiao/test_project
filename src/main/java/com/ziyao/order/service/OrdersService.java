package com.ziyao.order.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OrdersService {
	
	private final static String ORDERS = "orders";
	
	@Autowired
	private RestTemplate restTemplate;
	
    @Value("${orders.url}")
	String ordersUrl;
	
	public Map saveOrders(Map requestData) {
		ResponseEntity<Map> responseEntity = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map> request = new HttpEntity<>(requestData, headers);
		responseEntity = restTemplate.exchange(ordersUrl + ORDERS, HttpMethod.POST, request, Map.class);
		Map res = responseEntity.getBody();
		return res;
	}
}

package com.ziyao.shop.request;

import java.util.List;

import com.ziyao.shop.entity.Goods;

import lombok.Data;

@Data
public class ShoppingCartSaveReq {
	private Long cartId;
	private List<Goods> goods;
}

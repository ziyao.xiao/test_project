package com.ziyao.shop.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.ziyao.goods.service.GoodsService;
import com.ziyao.order.service.OrdersService;
import com.ziyao.shop.dao.CusShoppingCartMapper;
import com.ziyao.shop.dao.ShoppingCartMapper;
import com.ziyao.shop.entity.CusShoppingCart;
import com.ziyao.shop.entity.Goods;
import com.ziyao.shop.entity.ShoppingCart;
import com.ziyao.shop.request.ShoppingCartSaveReq;

@Service
public class ShopService {

	@Autowired
	private GoodsService goodsService;
	@Autowired
	private OrdersService ordersService;
	@Autowired
	private ShoppingCartMapper shoppingCartMapper;
	@Autowired
	private CusShoppingCartMapper cusShoppingCartMapper;
	
	public List<ShoppingCart> findAllShoppingCart(){
		return shoppingCartMapper.selectAll();
	}
	
	public ShoppingCart findByIdShoppingCart(Long id) {
		return shoppingCartMapper.selectByPrimaryKey(id);
	}

	public Boolean saveShoppingCart(ShoppingCart req) {
		return shoppingCartMapper.insert(req) > 0;
	}

	public Boolean updateShoppingCart(Long id, ShoppingCart req) {
		if(null != req) {
			req.setId(id);
		}
		return shoppingCartMapper.updateByPrimaryKey(req) > 0; 
	}
	
	public Boolean deleteShoppingCart(Long id) {
		return shoppingCartMapper.deleteByPrimaryKey(id) > 0;
	}
	
	public Map shoppingCartInit(String userId) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		CusShoppingCart cusShoppingCart = new CusShoppingCart();
		cusShoppingCart.setUserId(userId);
		cusShoppingCartMapper.insert(cusShoppingCart);
		resultMap.put("cartId", cusShoppingCart.getCartId());
		return resultMap;
	}
	
	public List<CusShoppingCart> shoppingCartFindByUserId(String userId) {
		return cusShoppingCartMapper.findByUserId(userId);
	}
	
	public List<ShoppingCart> shoppingCartInfo(Long cartId) {
		return shoppingCartMapper.findByCartId(cartId);
	}
	
	public Boolean shoppingCartSave(ShoppingCartSaveReq req) {
		Boolean result = false;
		
		if(null != req && !CollectionUtils.isEmpty(req.getGoods())) {
			for(Goods goods : req.getGoods()) {
				ShoppingCart cart = new ShoppingCart();
				BeanUtils.copyProperties(goods, cart);
				cart.setCartId(req.getCartId());
				shoppingCartMapper.insert(cart);
				result = true;
			}
		}
		return result;
	}
	
	public Map shoppingCartSubmitOrder(Long cartId) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		List<ShoppingCart> shoppingCartList = shoppingCartMapper.findByCartId(cartId);
		CusShoppingCart cusShoppingCart = cusShoppingCartMapper.selectByPrimaryKey(cartId);
		
		Map<String, Object> req = new HashMap<String, Object>();
		if(!CollectionUtils.isEmpty(shoppingCartList)) {
			List<Map<String, Object>> goodsList = new ArrayList<Map<String, Object>>();
			Map<String, Integer> goodsInventoryMap = new HashMap<String, Integer>();
			int totalAmount = 0;
			for(ShoppingCart cart : shoppingCartList) {
				Map<String, Object> goods = new HashMap<String, Object>();
				goods.put("count", cart.getCount());
				goods.put("goodsId", cart.getGoodsId());
				
				
				// 取得商品資訊
				Map goodsInfo = goodsService.findByIdGoods(cart.getGoodsId());
				Integer inventory = (Integer)goodsInfo.get("inventory");
				goods.put("goodsName", goodsInfo.get("goodsName"));
				goods.put("unitPrice", goodsInfo.get("unitPrice"));
				goodsList.add(goods);
				
				totalAmount += cart.getCount() * (Integer)goodsInfo.get("unitPrice");
				goodsInventoryMap.put(cart.getGoodsId(), inventory);
				// 更新庫存
				inventory = inventory - cart.getCount();
				Map<String, Object> goodsInventoryReq = new HashMap<String, Object>();
				goodsInventoryReq.put("inventory", inventory);
				goodsService.updateGoodsInventory(cart.getGoodsId(), goodsInventoryReq);
			}
			req.put("cartId", cartId);
			req.put("customerName", cusShoppingCart.getUserId());
			req.put("goods", goodsList);
			req.put("totalAmount" , totalAmount);
				
			try {	
				// 訂單
				Map saveOrdersResp = ordersService.saveOrders(req);
				resultMap.put("orderId", saveOrdersResp.get("orderId"));
				resultMap.put("result", "success");
			} catch(Exception e) {
				e.printStackTrace();
				recoveryGoodsInventory(goodsInventoryMap);
				resultMap.put("result", "error");
			}
		}
		return resultMap;
	}
	
	public void recoveryGoodsInventory(Map<String, Integer> goodsInventoryMap) {
		if(!CollectionUtils.isEmpty(goodsInventoryMap)) {
			for(Map.Entry<String, Integer> goodsInventory : goodsInventoryMap.entrySet()) {
				Map<String, Object> goodsInventoryReq = new HashMap<String, Object>();
				goodsInventoryReq.put("inventory", goodsInventory.getValue());
				goodsService.updateGoodsInventory(goodsInventory.getKey(), goodsInventoryReq);
			}
		}
	}
}




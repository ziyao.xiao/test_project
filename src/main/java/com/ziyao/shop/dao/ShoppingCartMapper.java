package com.ziyao.shop.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ziyao.shop.entity.ShoppingCart;

@Mapper
public interface ShoppingCartMapper {

	int deleteByPrimaryKey(Long id);

	int insert(ShoppingCart record);

	ShoppingCart selectByPrimaryKey(Long id);

	List<ShoppingCart> selectAll();

	int updateByPrimaryKey(ShoppingCart record);

	List<ShoppingCart> findByCartId(Long id);
}

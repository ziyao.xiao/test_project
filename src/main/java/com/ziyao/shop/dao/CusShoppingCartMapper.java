package com.ziyao.shop.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ziyao.shop.entity.CusShoppingCart;

@Mapper
public interface CusShoppingCartMapper {
	
	int deleteByPrimaryKey(Long cartId);

	int insert(CusShoppingCart record);

	CusShoppingCart selectByPrimaryKey(Long cartId);

	List<CusShoppingCart> selectAll();

	List<CusShoppingCart> findByUserId(String userId);
}

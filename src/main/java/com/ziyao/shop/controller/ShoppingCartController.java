package com.ziyao.shop.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ziyao.shop.common.ShopResp;
import com.ziyao.shop.entity.ShoppingCart;
import com.ziyao.shop.service.ShopService;

import io.swagger.annotations.Api;

@Api(tags = "Shopping Cart Entity")
@RestController
@RequestMapping(path = "/shoppingCart/")
public class ShoppingCartController {

	@Autowired
	private ShopService shopService;
	
	@GetMapping("")
	public ShopResp findAllShoppingCart() {
		return ShopResp.success(shopService.findAllShoppingCart());
	}
	
	@GetMapping("/{id}")
	public ShopResp findByIdShoppingCart(Long id) {
		return ShopResp.success(shopService.findByIdShoppingCart(id));
	}
	
	@PostMapping("")
	public ShopResp saveShoppingCart(@RequestBody ShoppingCart req) {
		return ShopResp.success(shopService.saveShoppingCart(req));
	}
	
	@RequestMapping(value = "/{id}", method = { RequestMethod.PUT, RequestMethod.PATCH })
	public ShopResp updateShoppingCart(Long id, @RequestBody ShoppingCart req) {
		return ShopResp.success(shopService.updateShoppingCart(id, req));
	}
	
	@DeleteMapping("/{id}")
	public ShopResp deleteShoppingCart(Long id) {
		return ShopResp.success(shopService.deleteShoppingCart(id));
	}
	
}

package com.ziyao.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ziyao.shop.common.ShopResp;
import com.ziyao.shop.request.ShoppingCartSaveReq;
import com.ziyao.shop.service.ShopService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "Shop")
@RestController
@RequestMapping(path = "/shop/")
public class ShopController {

	@Autowired
	private ShopService shopService;
	
	@GetMapping("test")
	public String test() {
		return "test";
	}

	@ApiOperation(value = "購物車新增")
	@PostMapping("shoppingCart/init")
	public ShopResp shoppingCartInit(String userId) {
		return ShopResp.success(shopService.shoppingCartInit(userId));
	}
	
	@ApiOperation(value = "取得使用者的購物車")
	@GetMapping("shoppingCart/userCartList")
	public ShopResp shoppingCartFindByUserId(String userId) {
		return ShopResp.success(shopService.shoppingCartFindByUserId(userId));
	}
	
	@ApiOperation(value = "購物車資訊")
	@GetMapping("shoppingCart/cartInfo")
	public ShopResp shoppingCartInfo(Long cartId) {
		return ShopResp.success(shopService.shoppingCartInfo(cartId));
	}
	
	@ApiOperation(value = "購物車儲存")
	@PostMapping("shoppingCart/save")
	public ShopResp shoppingCartSave(@RequestBody ShoppingCartSaveReq req) {
		return ShopResp.success(shopService.shoppingCartSave(req));
	}
	
	@ApiOperation(value = "購物車送出訂單")
	@PostMapping("shoppingCart/submitOrder")
	public ShopResp shoppingCartSubmitOrder(Long cartId) {
		return ShopResp.success(shopService.shoppingCartSubmitOrder(cartId));
	}
}

package com.ziyao.shop.entity;

import lombok.Data;

@Data
public class CusShoppingCart {
	private Long cartId;
	private String userId;
}

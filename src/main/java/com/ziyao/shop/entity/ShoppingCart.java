package com.ziyao.shop.entity;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ShoppingCart {
	private Long id;
	private Long cartId;
	private String goodsId;
	private String goodsName;
	private BigDecimal unitPrice;
	private Integer count;
}

package com.ziyao.shop.entity;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Goods {
	private String goodsId;
	private String goodsName;
	private BigDecimal unitPrice;
	private Integer count;
}

package com.ziyao.shop.common;

import lombok.Data;

@Data
public class ShopResp {
	private int rtnCode;
	private Object data;
	
	public static ShopResp success(Object data) {
		ShopResp apiSuccess = new ShopResp();
        apiSuccess.setRtnCode(0);
        apiSuccess.setData(data);
        return apiSuccess;
    }
}
